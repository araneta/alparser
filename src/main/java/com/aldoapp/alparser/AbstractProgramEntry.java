/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aldoapp.alparser;

/**
 *
 * @author aldo
 */
public class AbstractProgramEntry {
    protected final int type;
    protected String name;

    protected AbstractProgramEntry(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
