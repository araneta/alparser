/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aldoapp.alparser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;

/**
 *
 * @author aldo
 */
public class ListenerOrientedParser {
    public Class parse(String code) {
        CharStream charStream = new org.antlr.v4.runtime.ANTLRInputStream(code);
        PhpLexer lexer = new PhpLexer(charStream);
        TokenStream tokens = new org.antlr.v4.runtime.CommonTokenStream(lexer);
        PhpParser parser = new PhpParser(tokens);
        final PhpProgramModule module = new PhpProgramModule();

        
        PhpParserBaseListener listener = new PhpParserBaseListener() {
            protected final Stack<PhpProgramFunction> functions = new Stack<>();
             @Override
            public void enterClassDeclaration( PhpParser.ClassDeclarationContext ctx) {
                //String className = ctx.className().getText();
                String className = ctx.identifier().getText();
                int y= 0;
                //ctx.method().forEach(method -> method.enterRule(methodListener));
                /*
                ctx.method().forEach(method -> method.enterRule(methodListener));
                Collection<Method> methods = methodListener.getMethods();
                parsedClass = new Class(className,methods);
                */
            }

            @Override
            public void enterFunctionDeclaration(PhpParser.FunctionDeclarationContext ctx) {
                super.enterFunctionDeclaration(ctx);

                PhpProgramFunction function = new PhpProgramFunction();
                function.setName(ctx.identifier().getText());

                for (PhpParser.FormalParameterContext param : ctx.formalParameterList().formalParameter()) {
                    String name = param.getText();
                    PhpParser.TypeHintContext typeHint = param.typeHint();
                }

                functions.push(function);
            }

            @Override
            public void exitFunctionDeclaration(PhpParser.FunctionDeclarationContext ctx) {
                super.exitFunctionDeclaration(ctx);

                PhpProgramFunction function = functions.pop();
                module.addEntry(function);
            }
        };

        parser.addParseListener(listener);
        //parser.htmlDocument();
        parser.phpBlock();
        
        
        
        ClassListener classListener = new ClassListener();
        parser.phpBlock().enterRule(listener);
        return classListener.getParsedClass();
    }

    class ClassListener extends PhpParserBaseListener {

        private Class parsedClass;

        @Override
        public void enterClassDeclaration(@NotNull PhpParser.ClassDeclarationContext ctx) {
            //String className = ctx.className().getText();
            String className = ctx.identifier().getText();
            MethodListener methodListener = new MethodListener();
            //ctx.method().forEach(method -> method.enterRule(methodListener));
            /*
            ctx.method().forEach(method -> method.enterRule(methodListener));
            Collection<Method> methods = methodListener.getMethods();
            parsedClass = new Class(className,methods);
            */
        }

        public Class getParsedClass() {
            return parsedClass;
        }
    }

    class MethodListener extends PhpParserBaseListener {

        private Collection<Method> methods;

        public MethodListener() {
            methods = new ArrayList<>();
        }

        @Override
        public void enterMethod(@NotNull PhpParser.MethodContext ctx) {
            String methodName = ctx.methodName().getText();
            InstructionListener instructionListener = new InstructionListener();
            ctx.instruction().forEach(instruction -> instruction.enterRule(instructionListener));
            Collection<Instruction> instructions = instructionListener.getInstructions();
            methods.add(new Method(methodName, instructions));
        }

        public Collection<Method> getMethods() {
            return methods;
        }
    }

    class InstructionListener extends PhpBaseListener {

        private Collection<Instruction> instructions;

        public InstructionListener() {
            instructions = new ArrayList<>();
        }

        @Override
        public void enterInstruction(@NotNull PhpParser.InstructionContext ctx) {
            String instructionName = ctx.getText();
            instructions.add(new Instruction(instructionName));
        }

        public Collection<Instruction> getInstructions() {
            return instructions;
        }
    }
}
