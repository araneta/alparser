/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aldoapp.alparser;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Stack;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;

/**
 *
 * @author aldo
 */


public class Main {
    
    
    public static void main(String[] args) throws IOException{
        String source1 = "/media/aldo/49909430-d2bd-4bcf-be1d-3c425a4013bf/apps/projects/NameGen/alparser/test.php";
        
        String code = new String(Files.readAllBytes(Paths.get(source1)), StandardCharsets.UTF_8)+""
                + "?>";
        System.out.println("Contents: " + code);
        
        CharStream charStream = new org.antlr.v4.runtime.ANTLRInputStream(code);
        PhpLexer lexer = new PhpLexer(charStream);
        TokenStream tokens = new org.antlr.v4.runtime.CommonTokenStream(lexer);
        PhpParser parser = new PhpParser(tokens);
        final PhpProgramModule module = new PhpProgramModule();

        
        PhpParserBaseListener listener = new PhpParserBaseListener() {
            protected final Stack<PhpProgramFunction> functions = new Stack<>();
             @Override
            public void enterClassDeclaration( PhpParser.ClassDeclarationContext ctx) {
                //String className = ctx.className().getText();                
                String className = ctx.identifier().getText();
                System.out.println("class name :"+className);
                int y= 0;
                
            }

            @Override
            public void enterFunctionDeclaration(PhpParser.FunctionDeclarationContext ctx) {
                super.enterFunctionDeclaration(ctx);

                PhpProgramFunction function = new PhpProgramFunction();
                function.setName(ctx.identifier().getText());

                for (PhpParser.FormalParameterContext param : ctx.formalParameterList().formalParameter()) {
                    String name = param.getText();
                    PhpParser.TypeHintContext typeHint = param.typeHint();
                }

                functions.push(function);
            }

            @Override
            public void exitFunctionDeclaration(PhpParser.FunctionDeclarationContext ctx) {
                super.exitFunctionDeclaration(ctx);

                PhpProgramFunction function = functions.pop();
                module.addEntry(function);
            }
        };

        parser.addParseListener(listener);
        //parser.htmlDocument();
        parser.htmlDocument().enterRule(listener);
        

    }
}
